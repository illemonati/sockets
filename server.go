package main

import (
	"fmt"
	"net"
)

func server(port int) {
	conn, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", port))
	if err != nil {
		return
	}
	for {
		client, err := conn.Accept()
		if err != nil {
			return
		}
		client.Write([]byte(client.RemoteAddr().String()))
		msg := make([]byte, 1024)
		client.Read(msg)
		fmt.Println(string(msg))
	}
}

func main() {
	server(12345)
}
