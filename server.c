#include<stdlib.h>
#include<stdio.h>
#include<sys/socket.h>
#include<stdbool.h>
#include<arpa/inet.h>
#include<stdint.h>

void server(uint16_t port) {
    int s = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in server;
    server.sin_family = AF_INET;
    server.sin_port = htons(port);
    server.sin_addr.s_addr = INADDR_ANY;
    bind(s, (struct sockaddr *)&server, sizeof(server));
    listen(s, 10);
    struct sockaddr_in client;
    while (true) {
        int c = sizeof(struct sockaddr_in);
        int client_socket = accept(s, (struct sockaddr *)&client, (socklen_t *)&c);

        char client_address_only[INET_ADDRSTRLEN];
        inet_ntop(AF_INET, &client.sin_addr, client_address_only, INET_ADDRSTRLEN);

        char client_address_and_port[INET_ADDRSTRLEN+7];
        sprintf(client_address_and_port, "%s:%d", client_address_only, client.sin_port);
        send(client_socket, client_address_and_port, sizeof(client_address_and_port), 0);
        char recv_msg[1024];
        recv(client_socket, recv_msg, 1024, 0);
        printf("%s\n", recv_msg);
    }
}



int main() {
    server(12345);
    return 0;
}


