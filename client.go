package main

import (
	"fmt"
	"net"
	"time"
)

func connect(ip string, port int) {
	conn, err := net.Dial("tcp", fmt.Sprintf("%s:%d", ip, port))
	if err != nil {
		time.Sleep(time.Millisecond * 500)
		connect(ip, port)
		return
	}
	msg := make([]byte, 1024)
	conn.Read(msg)
	fmt.Println(string(msg))
	conn.Write([]byte("hi\n"))
	conn.Close()
}

func main() {
	connect("localhost", 12345)
}
