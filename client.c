#include<stdlib.h>
#include<stdio.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<string.h>
#include<unistd.h>
#include<stdint.h>

void client(char address[], uint16_t port) {
    struct sockaddr_in server;
    int s = socket(AF_INET, SOCK_STREAM, 0);
    if (s == -1) {
        printf("%d", 1);
        return;
    }
    server.sin_addr.s_addr = inet_addr(address);
    server.sin_family = AF_INET;
    server.sin_port = htons(port);
    if (connect(s, (struct sockaddr *)&server, sizeof(server)) < 0) {
        printf("%d", 2);
        return;
    }
    // printf("connected\n");


    char recv_msg[1024];
    recv(s, recv_msg, 1024, 0);
    printf("%s\n", recv_msg);

    char msg[] = "hello";
    send(s, msg, strlen(msg), 0);
    close(s);
}


int main() {
    client("127.0.0.1", 12345);
    return 0;
}

