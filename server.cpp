#include<iostream>
#include<boost/asio.hpp>

using boost::asio::ip::tcp;
using namespace std;

void server(unsigned short port) {
    boost::asio::io_service service;
    tcp::acceptor tcp_acceptor = tcp::acceptor(service, tcp::endpoint(tcp::v4(), port));
    while (true) {
        tcp::socket socc = tcp::socket(service);
        tcp_acceptor.accept(socc);
        string address = socc.remote_endpoint().address().to_string() + ":" + to_string(socc.remote_endpoint().port());
        boost::asio::write(socc, boost::asio::buffer(address));
        boost::asio::streambuf recv_buf;
        boost::asio::read(socc, recv_buf, boost::asio::transfer_at_least(1));
        const char *recv_msg = boost::asio::buffer_cast<const char *>(recv_buf.data());
        cout << recv_msg << endl;
    }
}


int main() {
    server(12345);
    return 0;
}
