import socket
import time

def connect(ip: str, port: int):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    while True:
        try:
            s.connect((ip, port))
            break
        except ConnectionError:
            time.sleep(0.5)
    msg = s.recv(1024)
    print(msg.decode())
    s.send(b'hi\n')
    s.close()


if __name__ == "__main__":
    connect('localhost', 12345)


