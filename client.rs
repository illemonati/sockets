use std::net::TcpStream;
use std::time::Duration;
use std::io::{Write, Read};
use std::thread::sleep;

fn connect(ip: impl Into<String>, port: impl Into<usize>) {
    let ip = ip.into();
    let port = port.into();
    let mut stream = match TcpStream::connect(format!("{}:{}", ip, port)) {
        Ok(s) => s,
        Err(_) => {
            sleep(Duration::from_millis(500));
            connect(ip, port); 
            return
            },
    };
    let mut msg = [0; 1024];
    stream.read(&mut msg).unwrap();
    println!("{}", std::str::from_utf8(&msg).unwrap());
    stream.write("hi\n".as_bytes()).unwrap();
}


fn main() {
    connect("localhost", 12345usize)
}


