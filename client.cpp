#include<iostream>
#include<boost/asio.hpp>

using namespace boost::asio;
using ip::tcp;
using namespace std;

void client(string addr, unsigned short port) {
    io_service ioservice;
    tcp::socket socc = tcp::socket(ioservice);
    socc.connect(tcp::endpoint(boost::asio::ip::address::from_string(addr), port));
    boost::asio::streambuf recv_buf;
    boost::asio::read(socc, recv_buf, transfer_at_least(1));
    const char *recv_msg = buffer_cast<const char *>(recv_buf.data());
    cout << recv_msg << endl;

    write(socc, buffer("hi"));
    socc.close();

}

int main() {
    client("127.0.0.1", 12345);
    return 0;
}

