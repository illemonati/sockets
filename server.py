import socket


def server(port: int, backlog: int):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('', port))
    s.listen(backlog)
    while True:
        c, addr = s.accept()
        c.send("{}:{}".format(addr[0], addr[1]).encode())
        msg = c.recv(1024)
        print(msg.decode())


if __name__ == "__main__":
    server(12345, 5)