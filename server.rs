use std::net::{TcpListener, TcpStream};
use std::io::{Write, Read};

fn server(port: impl Into<usize>) {
    let port = port.into();
    let listener = TcpListener::bind(format!("localhost:{}", port)).unwrap();
    while true {
        let (mut stream, addr) = listener.accept().unwrap();
        stream.write(&format!("{}", addr).as_bytes());
        let mut msg = [0; 1024];
        stream.read(&mut msg);
        println!("{}", std::str::from_utf8(&msg).unwrap());
    }
}



fn main() {
    server(12345usize);
}

